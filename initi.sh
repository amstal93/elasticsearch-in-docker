
#!/bin/bash
echo "memory should be increased to a min of 262144. please allow this, \n"
echo "otherwise, elasticsearch won't be able to run.\n"
sudo sysctl -w vm.max_map_count=262144